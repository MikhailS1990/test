
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<form method="get" name="filter">
        		<label for="date_from"> C </label>
        		<input type="date" name="date_from">
	            <label for="date_to"> По</label>
	            <input type="date" name="date_to">
	            <label for="order_state"> Статус</label>
	            <select name="order_state">
	            	<option value="">Все</option>
	            	@foreach ($states as $state)
	            		<option value="{{$state->state_id}}">{{$state->state_name}}</option>
	            	@endforeach
	            </select>
	            <label for="order_client_phone"> Телефон</label>
	            <input type="text" name="order_client_phone"></input>
	            <label for="order_good"> Товар</label>
	            <input type="text" name="order_good"></input>
	            <label for="order_id"> ID заказа</label>
	            <input type="text" name="order_id"></input>

        	</form>
        	<label for="find"> Поиск</label>
        	<input type="text" name="find">
            <div class="panel panel-default">
	            <label for="paginate"> Показывать по</label>
	            <select name="paginate">
	            	<option value="1">1</option>
	            	<option value="2" selected>2</option>
	            	<option value="3">3</option>
	            	<option value="10">10</option>
	            </select>
	            <div id="update">
	            	<table class="table">
					  <thead>
					    <tr>
					      <th data-order-name="order_id" data-order-direction="asc">#<i class="fa fa-sort active" aria-hidden="true"></i></th>
					      <th data-order-name="order_client_name" data-order-direction="asc">Клиент<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_client_phone" data-order-direction="asc">Телефон<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_good" data-order-direction="asc">Товар<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_state" data-order-direction="asc">Статус<i class="fa fa-sort" aria-hidden="true"></i></th>
					    </tr>
					  </thead>
					  <tbody>
						  @foreach ($orders as $order)
						    <tr>
						      <td>{{$order->order_id}}</td>
						      <td>{{$order->order_client_name}}</td>
						      <td>{{$order->order_client_phone}}</td>
						      <td>{{$order->good->good_name}}<br>{{$order->good->advert->user_first_name}} {{$order->good->advert->user_last_name}} ({{$order->good->advert->user_login}})</td>
						      <td>{{$order->state->state_name}}</td>
						    </tr>
						   @endforeach
					  </tbody>
					</table>
					{!! $orders->render() !!}
	            </div>
            </div>
        </div>
    </div>
</div>
@endsection

