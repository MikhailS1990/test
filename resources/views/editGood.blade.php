
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<form method="POST" action="{{ route('good.update', $good->good_id) }}" accept-charset="UTF-8">
					<input type="hidden" name="_method" value="PUT" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<label for="good_id">ID</label>
					<input type="text" name="good_id" disabled="true" value="{{$good->good_id}}"></input>
					<label for="good_advert">Рекламодатель</label>
					<select name='good_advert'>
						@foreach ($adverts as $advert)
							<option {{ $advert->user_id == $good->good_advert ? 'selected' : '' }} value="{{$advert->user_id}}">{{$advert->user_first_name}} {{$advert->user_last_name}}/{{$advert->user_login}}</option>
						@endforeach
					</select>
					<label for="good_name">Название</label>
					<input type="text" name="good_name" value="{{$good->good_name}}"></input>
					<label for="good_price">Цена</label>
					<input type="text" name="good_price" value="{{$good->good_price}}"></input>
					<input type="submit" value="Сохранить"></input>
				</form>
			</div>
        </div>
    </div>
</div>
@endsection