					<table class="table">
					  <thead>
					    <tr>
					      <th data-order-name="order_id" data-order-direction="asc">#<i class="fa fa-sort active" aria-hidden="true"></i></th>
					      <th data-order-name="order_client_name" data-order-direction="asc">Клиент<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_client_phone" data-order-direction="asc">Телефон<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_good" data-order-direction="asc">Товар<i class="fa fa-sort" aria-hidden="true"></i></th>
					      <th data-order-name="order_state" data-order-direction="asc">Статус<i class="fa fa-sort" aria-hidden="true"></i></th>
					    </tr>
					  </thead>
					  <tbody>
						  @foreach ($orders as $order)
						    <tr>
						      <td>{{$order->order_id}}</td>
						      <td>{{$order->order_client_name}}</td>
						      <td>{{$order->order_client_phone}}</td>
						      <td>{{$order->good->good_name}}<br>{{$order->good->advert->user_first_name}} {{$order->good->advert->user_last_name}} ({{$order->good->advert->user_login}})</td>
						      <td>{{$order->state->state_name}}</td>
						    </tr>
						   @endforeach
					  </tbody>
					</table>
					{!! $orders->render() !!}