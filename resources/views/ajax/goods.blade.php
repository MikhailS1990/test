	            	<table class="table">
					  <thead>
					    <tr>
					    	<th></th>
					      	<th data-order-name="good_id" data-order-direction="asc">#<i class="fa fa-sort active" aria-hidden="true"></i></th>
					      	<th data-order-name="good_name" data-order-direction="asc">Название<i class="fa fa-sort" aria-hidden="true"></i></th>
					      	<th data-order-name="order_client_phone" data-order-direction="asc">Цена<i class="fa fa-sort" aria-hidden="true"></i></th>
					      	<th data-order-name="order_good" data-order-direction="asc">Рекламодатель<i class="fa fa-sort" aria-hidden="true"></i></th>
					    </tr>
					  </thead>
					  <tbody>
						  @foreach ($goods as $index=>$good)
						    <tr>
						    	<td><input name ="good_select[]" type="checkbox" value="{{$good->good_id}}"></td>
						      	<td scope="row">{{$index+1}}</td>
						      	<td><a href="{{ route('good.edit', $good->good_id) }}">{{$good->good_name}}</a> <br> Внешний ID:{{$good->good_id}}</td>
						      	<td>{{$good->good_price}}</td>
						      	<td>{{$good->advert->user_first_name}} {{$good->advert->user_last_name}}<br>{{$good->advert->user_login}})</td>
						    </tr>
						   @endforeach
					  </tbody>
					</table>
					{!! $goods->render() !!}