var page_number='1';
var update = function(){
	order_by_name=$('.fa-sort.active').closest('th').attr('data-order-name')
	order_by_direction=$('.fa-sort.active').closest('th').attr('data-order-direction')
	$.ajax({
		data: $('form').serialize()+"&find="+$('input[name="find"]').val()+"&paginate="+$('select[name="paginate"]').val()+"&page="+page_number+ "&order_by_name=" + order_by_name+ "&order_by_direstion=" + order_by_direction,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
	   	success: function(data){
	      	$('#update').empty().append(data);
	      	$('.fa-sort').removeClass('active');
			$('th[data-order-name='+order_by_name+']').find('.fa-sort').addClass('active');
	   	}
	});
	
}
$(document).ready(function(){
	$('body').on('change','input,select',function(){
		update();
	})
	$('input').keyup(function(){
		update();
	})
	$('body').on('click','th',function(){
		if($(this).find('.active').length){
			if($(this).attr('data-order-direction')=='asc'){
				$(this).attr('data-order-direction','desc')
			}
			else
			{
				$(this).attr('data-order-direction','asc')
			}
		}else{
			$('.fa-sort').removeClass('active');
			$(this).find('.fa-sort').addClass('active');
		}
		update();
	})
	$('body').on('click','.pagination a',function(){
		event.preventDefault();
    	page_number = $(this).text();
    	update();
	})
})