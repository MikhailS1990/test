<?php
use App\State;
use App\Advert;
use App\Good;
use App\Order;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();
        State::create(array('state_name' => 'Новый','state_slug' => 'new'));
        State::create(array('state_name' => 'В работе','state_slug' => 'onoperator'));
        State::create(array('state_name' => 'Подтвержден','state_slug' => 'accepted'));
        DB::table('adverts')->delete();
        Advert::create(array('user_first_name' => 'John','user_last_name' => 'Doe','user_login' => 'jdoe@gmail.com','user_password' => bcrypt("password")));
		Advert::create(array('user_first_name' => 'Erick','user_last_name' => 'Doe','user_login' => 'edoe@gmail.com','user_password' => bcrypt("password")));
		Advert::create(array('user_first_name' => 'Michel','user_last_name' => 'Doe','user_login' => 'mdoe@gmail.com','user_password' => bcrypt("password")));
		DB::table('goods')->delete();
		Good::create(array('good_name' => 'Часы Rado Integral','good_price' => 2000,'good_advert' => 1));
		Good::create(array('good_name' => 'Часы Swiss Army','good_price' => 1500,'good_advert' => 1));
		Good::create(array('good_name' => 'Детский планшет','good_price' => 2100,'good_advert' => 2));
		Good::create(array('good_name' => 'Колонки Monster Beats','good_price' => 900,'good_advert' => 3));
		DB::table('orders')->delete();
		Order::create(array('order_state' => 1,'order_good' => 1,'order_client_phone' => '777713522547','order_client_name' => 'John'));
		Order::create(array('order_state' => 2,'order_good' => 2,'order_client_phone' => '77756547656','order_client_name' => 'Michel'));
		Order::create(array('order_state' => 3,'order_good' => 3,'order_client_phone' => '77786789878','order_client_name' => 'Darrel'));
		Order::create(array('order_state' => 3,'order_good' => 4,'order_client_phone' => '77752456523','order_client_name' => 'Dan'));
    }
}
