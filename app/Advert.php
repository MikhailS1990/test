<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Advert extends Authenticatable
{
	public $timestamps = false;
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_first_name', 'user_last_name', 'user_login',
    ];
    protected $hidden = [
        'user_password', 'remember_token',
    ];
    public function getAuthPassword()
    {
        return $this->user_password;
    }
    public function goods()
    {
        return $this->hasMany('App\Good','good_advert','good_id');
    }

}
