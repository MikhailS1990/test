<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'state_id';
    public function orders()
    {
        return $this->hasMany('App\Order', 'order_state', 'state_id');
    }
}
