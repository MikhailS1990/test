<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Order;
use App\State;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {	
    	if(Input::has("paginate")) {
    		$paginate = Input::get("paginate");
    	} else{
    		$paginate= 2;
    	}
    	if(Input::has("order_by_name")) {
    		$order_by_name = Input::get("order_by_name");
    	} else{
    		$order_by_name = 'order_id';
    	}
    	if(Input::has("order_by_direstion")) {
    		$order_by_direction = Input::get("order_by_direstion");
    	} else{
    		$order_by_direction = 'asc';
    	}
        $orders=Order::orderBy($order_by_name, $order_by_direction);
        
        if(Input::has("order_state")) {
            $orders=$orders->where('order_state','=',Input::get("order_state"));
        }
        if(Input::has("date_from")) {
            $orders=$orders->whereDate('order_add_time','>=',Input::get("date_from"));
        }
        if(Input::has("date_to")) {
            $orders=$orders->whereDate('order_add_time','<=',Input::get("date_to"));
        }
        if(Input::has("order_client_phone")) {
            $orders=$orders->where('order_client_phone','like',Input::get("order_client_phone").'%');
        }
        if(Input::has("find")) {
            $orders=$orders->where('order_client_phone','like',Input::get("find").'%')->orWhere('order_client_name','like',Input::get("find").'%');
        }
        if(Input::has("order_good")) {
            $order_good=Input::get("order_good");
            $orders=$orders->whereHas('good', function($q) use ($order_good)
            {
                $q->where('good_name', 'like', $order_good.'%');

            });
        }
        if(Input::has("order_id")) {
            $orders=$orders->where('order_id','=',Input::get("order_id"));
        }
        $orders=$orders->paginate($paginate);
        if ($request->ajax())
        {
            return view('ajax.orders', ['orders' => $orders]);
        }else{
            $states=State::get();
            return view('orders', ['orders' => $orders,'states'=>$states]);
        }
    }
}
