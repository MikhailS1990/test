<?php

namespace App\Http\Controllers\Auth;

use App\Advert;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $username = 'user_login';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getAuthPassword() 
    {
       return $this->user_password; 
    }
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'user_login' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('user_login', 'password');

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath())
            ->withInput($request->only('user_login', 'remember'))
            ->withErrors([
                'user_login' => $this->getFailedLoginMessage(),
            ]);
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_login' => 'required|email|max:255|unique:adverts',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
