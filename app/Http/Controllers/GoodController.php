<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Good;
use App\Advert;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
class GoodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function index(Request $request)
    {
    	if(Input::has("paginate")) {
    		$paginate = Input::get("paginate");
    	} else{
    		$paginate= 2;
    	}
    	if(Input::has("order_by_name")) {
    		$order_by_name = Input::get("order_by_name");
    	} else{
    		$order_by_name = 'good_id';
    	}
    	if(Input::has("order_by_direstion")) {
    		$order_by_direction = Input::get("order_by_direstion");
    	} else{
    		$order_by_direction = 'asc';
    	}
        $goods=Good::orderBy($order_by_name, $order_by_direction)->paginate($paginate);;
        if ($request->ajax())
        {
            return view('ajax.goods', ['goods' => $goods]);
        }else{
            return view('goods', ['goods' => $goods]);
        }
    }
    public function edit($id)
    {	
    	$adverts=Advert::get();
    	$good=Good::find($id);
    	return view('editGood', ['good' => $good,'adverts'=>$adverts]);
    }
    public function update($id)
    {	
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'good_advert'       => 'required',
            'good_name'      => 'required',
            'good_price' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('good/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $good = Good::find($id);
            $good->good_advert       = Input::get('good_advert');
            $good->good_name      = Input::get('good_name');
            $good->good_price = Input::get('good_price');
            $good->save();

            // redirect
            return Redirect::to('good');
        }
    }
}
