<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
	public $timestamps = false;
    protected $primaryKey = 'good_id';
    public function advert()
    {
        return $this->belongsTo('App\Advert', 'good_advert', 'user_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order', 'order_good', 'good_id');
    }
}
